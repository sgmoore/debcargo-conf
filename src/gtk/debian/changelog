rust-gtk (0.18.1-4) unstable; urgency=medium

  * Drop unnecessary patch for gir-format-check 

 -- Matthias Geiger <werdahias@riseup.net>  Thu, 23 Nov 2023 21:49:44 +0100

rust-gtk (0.18.1-3) unstable; urgency=medium

  * Team upload
  * Package gtk 0.18.1 from crates.io using debcargo 2.6.0
  * Release to unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Thu, 28 Sep 2023 15:37:52 -0400

rust-gtk (0.18.1-2) experimental; urgency=medium

  * Specify dependency on rustc 1.70

 -- Matthias Geiger <werdahias@riseup.net>  Sat, 16 Sep 2023 13:04:33 +0200

rust-gtk (0.18.1-1) experimental; urgency=medium

  * Package gtk 0.18.1 from crates.io using debcargo 2.6.0
  * Regenerate source code with debian tools before build
  * Rebased patches

 -- Matthias Geiger <werdahias@riseup.net>  Fri, 15 Sep 2023 23:01:05 +0200

rust-gtk (0.17.1-1) unstable; urgency=medium

  * Package gtk 0.17.1 from crates.io using debcargo 2.6.0

  [ Peter Michael Green ]
  * Team upload.
  * Package gtk 0.16.2 from crates.io using debcargo 2.6.0
  * Fix comment in debian/rules.

  [ Matthias Geiger ]
  * Removed inactive uploader, added my new mail address
  * Added relax-gir-dep.diff patch to enable tests

 -- Matthias Geiger <werdahias@riseup.net>  Wed, 02 Aug 2023 00:22:53 +0200

rust-gtk (0.16.2-3) unstable; urgency=medium

  * Team upload.
  * Package gtk 0.16.2 from crates.io using debcargo 2.6.0
  * Disable build-time testing on mipsel, they run out of address space.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 29 Jun 2023 00:46:58 +0000

rust-gtk (0.16.2-2) unstable; urgency=medium

  * Package gtk 0.16.2 from crates.io using debcargo 2.6.0

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Sun, 25 Jun 2023 21:01:03 +0200

rust-gtk (0.16.2-1) experimental; urgency=medium

  * Package gtk 0.16.2 from crates.io using debcargo 2.6.0

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Sat, 20 May 2023 15:17:16 +0200

rust-gtk (0.14.3-1) unstable; urgency=medium

  * Team upload.
  * Package gtk 0.14.3 from crates.io using debcargo 2.5.0 (Closes: 1002205)
  * Disable test that requires a display.
  * Mark tests for the "dox" feature as broken, from some searching it seems
    the feature is only intended to be used when builing documentation, not
    when actually compiling the package.
  * Disable check_gir test, it seems to pass when run manually but fail
    in autopkgtest.

  [ Henry-Nicolas Tourneur ]
  * Package gtk 0.14.3 from crates.io using debcargo 2.4.4
  * d/patches: removed patch remove-futures-preview-feature.diff

 -- Peter Michael Green <plugwash@debian.org>  Wed, 29 Dec 2021 03:17:19 +0000

rust-gtk (0.7.0-1) unstable; urgency=medium

  * Team upload.
  * Package gtk 0.7.0 from crates.io using debcargo 2.4.0

  [ Wolfgang Silbermayr ]
  * Package gtk 0.7.0 from crates.io using debcargo 2.3.0

 -- Ximin Luo <infinity0@debian.org>  Thu, 15 Aug 2019 19:14:02 -0700

rust-gtk (0.5.0-2) unstable; urgency=medium

  * Package gtk 0.5.0 from crates.io using debcargo 2.2.9
  * Patch out futures preview from gtk

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Tue, 22 Jan 2019 09:43:50 +0100

rust-gtk (0.5.0-1) unstable; urgency=medium

  * Package gtk 0.5.0 from crates.io using debcargo 2.2.9

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Wed, 26 Dec 2018 14:51:23 -0800
